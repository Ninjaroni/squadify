import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import About from '@/components/About'
import Search from '@/components/Search'
import Callback from '@/components/Callback'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path: '/callback',
      name: 'callback',
      component: Callback
    },
    {
      path: '*', redirect: '/'
    }
  ]
})
