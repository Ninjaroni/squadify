# Squadify.gg

> A small application that enables players and teams to find each other.

## Build Setup

``` bash
MAKE SURE YOU'RE USING NODE v8.9.4

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# serve into prod environment .via express (make sure you've ran 'npm run build' first)
npm run prod
```
